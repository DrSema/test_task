#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time, requests, random
#import threading

#Варианты запросов
endpoints = ('', 'summ?a=54&b=99"', 'skip', 'other', '404', 'error')

#Непрерывано шлем get запросы одним из случайниых варинтов к app_flask, каждые 0,5 сек, в stdout выводим запрос, и успешен он или нет
def run():
    while True:
        try:
            target = random.choice(endpoints)
            requests.get("http://app_flask:5060/%s" % target, timeout=1)
            time.sleep(0.5)
            print ("sent %s"  % target)
        except:
            #при ошибке выводим варинт запроса и ждем 5 секунд
            print ("except %s"  % target)
            time.sleep(5)

if __name__ == '__main__':
    #for _ in range(1):
    #    thread = threading.Thread(target=run)
    #    thread.setDaemon(True)
    #    thread.start()

    #while True:
        run()
        #time.sleep(10)
