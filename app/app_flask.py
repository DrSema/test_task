#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
How to use:
curl "http://localhost:5060/summ?a=54&b=99" 
or open URL in browser
"""
import time, random
from flask import Flask, request
from prometheus_flask_exporter import PrometheusMetrics

app = Flask(__name__)
metrics = PrometheusMetrics(app)

# static information as metric
metrics.info('app_info', 'Application info', version='1.0.3')

# root URL path
@app.route('/')
def main(): 
    return 'ROOT dir, ok'

@app.route('/other')
def other(): 
    time.sleep(random.uniform(0.248, 0.253))
    return 'other dir, 1 second, ok'

@app.route('/error')
def error(): 
    return 'Error, ok', 500

# URL/skip path
@app.route('/skip')
@metrics.do_not_track() # default metrics are not collected
def skip():
    return 'skip'

# URL/summ path, main func this app
@app.route('/summ', methods=['GET', 'POST'])
def read_req():
    a = request.args['a']
    b = request.args['b']
    c = summ(a,b)
    #print request.environ.get('HTTP_USER_AGENT')
    #detect curl
    if ("curl" in request.environ.get('HTTP_USER_AGENT')):
        return ('a=%s\nb=%s\na+b=%s\nCURL detect\n' % (a,b,c))
    else:
        return ('a=%s<p>b=%s<p>a+b=%s<p>' % (a,b,c))

# Summ func, check params and summ if they are INT numbers
def summ(a,b):
    if (a.isdigit() == False or b.isdigit() == False):
        return ('ERROR!!! a or b - not INT nubers')
    else:
        return (int(a)+int(b))

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5060)

