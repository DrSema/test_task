Автоматическое разворачиванеи python flask приложения с передачей метрик в prometheus и визуализацией в Graphana по преднастроенным заранее дашбоардам

**app**  
app_flask.py - само приложение  
req_generator.py - приложение для генерации обращений к app_flask  
Dockerfile и requirements.txt для создания контейнероВ (один сервис - один контейнер) сразу для обоих приложений

**grafana**  
dashboards - преднастроенные дашбоарды под grafana для app_flask и node_exporter хостовой машины  
datasources - преднастроенный источник данных prometheus  
setup.sh - скрит добавления дашбоардов и источников данных используя grafana API  

**prometheus**  
prometheus.yml - конфиг prometheus с указанными хостами для сбора данных  

**docker-compose.yml** - для запуска всего одновременно
